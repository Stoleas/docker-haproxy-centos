# docker build -t haproxy:latest .
FROM centos:latest
MAINTAINER stoleas

RUN yum -y -q update
RUN yum -y -q install haproxy
RUN yum clean all

RUN mv /etc/haproxy/haproxy.cfg /etc/haproxy/haproxy.cfg.org 
ADD etc/haproxy/haproxy.cfg /etc/haproxy/haproxy.cfg 

# docker run --expose 80 -p 80:80  -ti haproxy:latest /bin/bash
# /usr/sbin/haproxy -f /etc/haproxy/haproxy.cfg
